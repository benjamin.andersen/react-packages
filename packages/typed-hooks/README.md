# typed-hooks / alpha

A collection of simple, typed React hooks.

## Installation:

Simply install by running `npm i react @elohmrow/typed-hooks`.

*Note: this repository requires React v17+ as a peer dependency.*

## Typed Hooks API: *Coming Soon*

[0]: #useNullishRef()
[1]: #useNullishState()
[2]: #useToggle()
[3]: #useLoader()
[4]: #useLazyEffect()
[5]: #useTimeout()
[6]: #useInterval()
[7]: #useEventListener()
[8]: #usePreventUnload()
[9]: #useHover()
