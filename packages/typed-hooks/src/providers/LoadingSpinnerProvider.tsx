import {createContext, useCallback, useState} from 'react'

import useScrollLock from '../hooks/useScrollLock'

interface LoadingSpinnerState {
    active: boolean
    addTotal: (amount: number) => void
}

interface LoadingSpinnerProviderProps {
    keepSpinnerMounted?: boolean
    noScrollLock?: boolean
    spinner: React.ReactNode | ((active: boolean) => React.ReactNode)
    children?: React.ReactNode
}

const initialState: LoadingSpinnerState = {
    active: false,
    addTotal: () => {}
}

export const LoadingSpinnerContext = createContext(initialState)

/**
 * NOTE: This should be used in conjunction with `useLoadingSpinner()`.
 *
 * It is recommended to place this at the top of the component tree.
 *
 * @param spinner The loading spinner or a function that returns it.
 * @param keepSpinnerMounted Optionally keep spinner mounted when loading state is not active.
 * @param noScrollLock Optionally disable automatic scroll lock when loading state is active.
 *
 * @example
 * ```tsx
 * // simple use-case
 * <LoadingSpinnerProvider spinner={<LoadingSpinner />}>
 *     <App />
 * </LoadingSpinnerProvider>
 *
 * // spinner component handles loading state internally
 * <LoadingSpinnerProvider
 *     keepSpinnerMounted
 *     spinner={(active) => <LoadingSpinner active={active} />}>
 *     <App />
 * </LoadingSpinnerProvider>
 * ```
 */

export const LoadingSpinnerProvider: React.FC<LoadingSpinnerProviderProps> = ({
    keepSpinnerMounted = false,
    noScrollLock = false,
    spinner: spinnerOrCreateSpinner,
    children
}) => {
    const [total, setTotal] = useState(0)
    const active = !!total

    const addTotal = useCallback((amount: number) => {
        setTotal((total) => Math.max(total + amount, 0))
    }, [])

    useScrollLock(!noScrollLock && active)

    const spinner =
        spinnerOrCreateSpinner instanceof Function
            ? spinnerOrCreateSpinner(active)
            : spinnerOrCreateSpinner

    return (
        <LoadingSpinnerContext.Provider value={{active, addTotal}}>
            {children}
            {keepSpinnerMounted || active ? spinner : null}
        </LoadingSpinnerContext.Provider>
    )
}

export default LoadingSpinnerProvider
