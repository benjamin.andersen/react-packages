import {useEffect, MutableRefObject} from 'react'

type EventName = keyof WindowEventMap
type EventHandler<K extends EventName> = (event: WindowEventMap[K]) => void

/**
 * Creates an event listener and removes it on unmount.
 *
 * @param eventName The {@link EventName event} name.
 * @param handler An {@link EventHandler event handler}.
 * @param ref A mutable {@link MutableRefObject ref}.
 */

export const useEventListener = <K extends EventName, T extends HTMLElement>(
    eventName: K,
    handler: EventHandler<K>,
    ref?: MutableRefObject<T | null>
) => {
    useEffect(() => {
        const targetElement = ref == null ? window : ref.current
        if (!targetElement) return

        const eventHandler: EventHandler<K> = (event) => handler(event)

        targetElement.addEventListener(eventName, eventHandler as EventListener)

        return () => {
            targetElement.removeEventListener(
                eventName,
                eventHandler as EventListener
            )
        }
    }, [handler])
}

export default useEventListener
