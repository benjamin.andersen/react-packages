import {useEffect} from 'react'

/**
 * A hook that conditionally prevents scrolling on the page.
 *
 * @param lockScroll Whether to lock scrolling on the page.
 */

export const useScrollLock = (lockScroll: boolean) => {
    useEffect(() => {
        const html = document.documentElement
        const cantScroll = html.clientHeight >= html.scrollHeight
        if (lockScroll && cantScroll) return

        document.documentElement.style.overflowY = lockScroll ? 'hidden' : ''

        return () => {
            document.documentElement.style.overflowY = ''
        }
    }, [lockScroll])
}

export default useScrollLock
