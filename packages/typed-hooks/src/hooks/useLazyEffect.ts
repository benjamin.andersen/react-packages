import {useEffect, EffectCallback, DependencyList} from 'react'
import useMountRef from './useMountRef'

/**
 * Identical to {@link useEffect}, except it does not run on mount.
 *
 * @param effect A `useEffect` effect.
 * @param deps A `useEffect` dependency list.
 */

export const useLazyEffect = (effect: EffectCallback, deps: DependencyList) => {
    const didMountRef = useMountRef()

    useEffect(() => {
        if (didMountRef) return effect()
    }, deps)
}

export default useLazyEffect
