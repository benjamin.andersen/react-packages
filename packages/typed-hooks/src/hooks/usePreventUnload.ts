import useEventListener from './useEventListener'

/**
 * A hook that prevents unloading the page if a condition is met.
 *
 * @param condition Prevent unload condition
 */
export const usePreventUnload = (condition: boolean) => {
    useEventListener('beforeunload', (e) => {
        if (!condition) return
        e.preventDefault()
        e.returnValue = true
    })
}

export default usePreventUnload
