import {MutableRefObject} from 'react'

export type CursorPosition = number | null
export type MaskValue = string | RegExp
export type MaskArray = MaskValue[]
export type Formatter = (value: string) => string
export type Mask = (value: string) => string
export type Validate = (value: string) => boolean
export type MaskNode = HTMLInputElement | null
export type MaskRef = MutableRefObject<MaskNode>
export type WeaveArrays = any[][]
export type Weave<T extends WeaveArrays> = T[number][number][]

export interface Chunk {
    type: 'mask' | 'user'
    value: string
}

export interface MaskOptions {
    mask: MaskArray
    format?: Formatter
}

export interface MaskProps {
    mask: Mask
    placeholder: string
    validate: Validate
    maskRef: MaskRef
}
