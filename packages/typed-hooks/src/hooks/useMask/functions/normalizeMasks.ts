import {MaskArray} from '../types'

/**
 * Converges repeated masks in a mask array.
 *
 * @param maskArray A {@link MaskArray mask array}.
 * @returns A normalized mask array.
 *
 * @example
 * ```
 * const maskArray = ['foo', 'bar', /baz/, /qux/]
 * const normalizedMasks = normalizeMasks(maskArray)
 *
 * // ['foobar', /bazqux/]
 * console.log(normalizedMasks)
 * ```
 */

export const normalizeMasks = (maskArray: MaskArray): MaskArray => {
    let newMaskArray = [...maskArray]
    let deleteCount = 0
    let maskCount = 0
    let regexCount = 0
    let prevMask = ''
    let prevRegex = new RegExp('')

    maskArray.forEach((mask, i) => {
        if (mask instanceof RegExp) {
            regexCount++
            const prevRegexSource = prevRegex?.source ?? ''
            prevRegex = new RegExp(prevRegexSource + mask.source)

            if (!maskCount) return

            newMaskArray.splice(
                i - maskCount - deleteCount,
                maskCount,
                prevMask
            )

            deleteCount += maskCount - 1
            maskCount = 0
            prevMask = ''
            return
        }

        maskCount++
        prevMask += mask

        if (!regexCount) return

        newMaskArray.splice(
            i - regexCount - deleteCount,
            regexCount,
            prevRegex!
        )

        deleteCount += regexCount - 1
        regexCount = 0
        prevRegex = new RegExp('')
    })

    return newMaskArray
}

export default normalizeMasks
