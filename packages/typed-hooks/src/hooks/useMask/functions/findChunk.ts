import {Chunk} from '../types'

/**
 * Finds the {@link Chunk chunk} whose preceeding length
 * is greater than the position.
 *
 * @param chunks An array of chunks.
 * @param position The cursor position.
 */

export const findChunk = (
    chunks: Chunk[],
    position: number
): Chunk | null => {
    let valueIndex = 0

    for (const chunk of chunks) {
        const chunkLength = chunk.value.length
        const chunkRange = chunkLength + valueIndex
        if (position < chunkRange) return chunk
        valueIndex += chunkLength
    }

    return null
}

export default findChunk
