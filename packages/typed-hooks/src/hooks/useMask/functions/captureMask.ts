import {MaskValue} from '../types'

/**
 * Encloses a {@link MaskValue mask} string in a capturing group.
 *
 * @param mask A mask string.
 * @returns A captured mask string.
 */

export const captureMask = (mask: string) => '(' + mask + ')'

export default captureMask
