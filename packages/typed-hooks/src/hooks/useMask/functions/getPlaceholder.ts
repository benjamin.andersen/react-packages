import {MaskArray} from '../types'
import {getMaskStrings} from '.'

/**
 * Filters strings from a {@link MaskArray mask array}
 * and joins it by whitespace.
 *
 * @param maskArray A mask array.
 * @returns A text field placeholder.
 */

export const getPlaceholder = (maskArray: MaskArray) => {
    const maskStrings = getMaskStrings(maskArray)
    return maskStrings.join(' ')
}

export default getPlaceholder
