import {MaskArray, Chunk} from '../types'

import {normalizeMasks, weave, getMaskStrings} from '.'

/**
 * Chunks a {@link MaskArray mask array} into mask values
 * and user inputs. The mask values are characters that mask the user inputs.
 *
 * Note: This utilizes {@link weave} internally. You should pass it a
 * {@link normalizeMasks normalized} mask array.
 *
 * @param maskArray A mask array.
 * @param userInput An array of user inputs.
 */

export const chunk = (maskArray: MaskArray, userInput: string[]): Chunk[] => {
    const maskStrings = getMaskStrings(maskArray)
    const firstChunkIsUser = maskArray[0] instanceof RegExp

    const maskChunks = maskStrings.map<Chunk>((value) => ({
        type: 'mask',
        value
    }))

    const userChunks = userInput.map<Chunk>((value) => ({
        type: 'user',
        value
    }))

    if (firstChunkIsUser) return weave(userChunks, maskChunks)
    else return weave(maskChunks, userChunks)
}

export default chunk
