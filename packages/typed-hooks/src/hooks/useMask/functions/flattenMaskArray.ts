import {MaskArray} from '../types'
import {escapeMask, captureMask} from '.'

/**
 * Flattens a mask array into a single regex.
 *
 * @param maskArray A {@link MaskArray mask array}
 * @returns A user input validation and capture regex.
 */

export const flattenMaskArray = (maskArray: MaskArray): RegExp => {
    const stringifiedMasks = maskArray.map((mask) => {
        if (mask instanceof RegExp) return captureMask(mask.source)
        return escapeMask(mask)
    })

    return new RegExp(stringifiedMasks.join(''))
}

export default flattenMaskArray
