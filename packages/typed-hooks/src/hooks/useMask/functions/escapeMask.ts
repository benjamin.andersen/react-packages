import {MaskValue} from '../types'

/**
 * Escapes special regex characters from a {@link MaskValue mask} string
 *
 * @param mask A mask string.
 * @returns An escaped mask string.
 */

export const escapeMask = (mask: string) =>
    mask.replace(/[\\^$.|?*+()[{]/g, '\\$&')

export default escapeMask
