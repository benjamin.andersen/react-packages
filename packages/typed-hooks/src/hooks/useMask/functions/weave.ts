import {WeaveArrays, Weave} from '../types'

/**
 * Weaves multiple arrays into one.
 *
 * @param arrays The arrays to weave.
 * @returns A {@link Weave weave} array.
 *
 * @example
 * ```
 * const foo = ['a', 'b', 'c', 'd']
 * const bar = [1, 2, 3]
 * const baz = [['a'], ['b'], ['c'], ['d'], ['e']]
 *
 * const weaved = weave(foo, bar, baz, qux)
 *
 * // ['a', 1, ['a'], 'b', 2, ..., 'd', ['d'], ['e']]
 * console.log(weaved)
 * ```
 */

export const weave = <T extends WeaveArrays>(...arrays: T): Weave<T> => {
    let weaved: T[] = []

    const arrayLengths = arrays.map((arr) => arr.length)
    const n = Math.max(...arrayLengths)

    for (let i = 0; i < n; i++) {
        arrays.forEach((arr) => weaved.push(arr[i]))
    }

    return weaved.filter(Boolean)
}

export default weave
