import {MaskValue, MaskArray} from '../types'

/**
 * Filters strings from a {@link MaskArray mask array}.
 *
 * @param maskArray A mask array.
 * @returns An array of {@link MaskValue mask} strings.
 */

export const getMaskStrings = (maskArray: MaskArray) =>
    maskArray.filter((mask): mask is string => !(mask instanceof RegExp))

export default getMaskStrings
