/**
 * Simulates a backspace given the cursor position and value.
 *
 * @param position The cursor position.
 * @param value The value to simulate deletion.
 *
 * @example
 * ```
 * const position = 2
 * const value = 'foo'
 * const simulatedValue = simulateBackspace(position, value)
 *
 * // 'fo|o' --> 'f|o'
 * console.log(simulatedValue)
 * ```
 */

export const simulateBackspace = (position: number, value: string) =>
    value.slice(0, position - 1) + value.slice(position)

export default simulateBackspace
