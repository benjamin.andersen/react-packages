const errors = {
    noFormatCoerce: new Error(
        'The format function must coerce the user input to the mask.'
    )
}

export default errors
