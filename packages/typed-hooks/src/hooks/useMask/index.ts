import {useState, useRef, useEffect} from 'react'

import {useEventListener} from '../'

import {
    CursorPosition,
    MaskArray,
    Formatter,
    Mask,
    Validate,
    MaskNode,
    MaskRef,
    MaskOptions,
    MaskProps
} from './types'

import {
    chunk,
    findChunk,
    simulateBackspace,
    getPlaceholder,
    flattenMaskArray,
    normalizeMasks
} from './functions'

import config from './config'

/**
 * Handles input masking by providing some useful props.
 *
 * @alpha
 * @note The mask ref **must** be applied to the input element you wish to mask.
 *
 * @param maskOptions -
 * - A {@link MaskArray mask array}
 * - A custom {@link Formatter format} function *(optional)*
 *
 * @returns
 * - A {@link Mask mask} function
 * - A {@link Validate validate} function
 * - An input placeholder
 * - A {@link MaskRef mask ref}
 *
 * @example
 * ```
 * import {useState} from 'react'
 * import {useMask} from '@elohmrow/typed-hooks'
 * import {TextField} from '@material-ui/core
 *
 * const App = () => {
 *     const [value, setValue] = useState('')
 *
 *     const {
 *         mask,
 *         validate,
 *         placeholder,
 *         maskRef
 *     } = useMask({
 *         mask: ['(', /\d{0,3}/, ') ', /\d{0,3}/, '-', /\d{0,4}/]
 *     })
 *
 *     const handleChange = (value: string) => {
 *         const maskedValue = mask(value)
 *         setValue(maskedValue)
 *     }
 *
 *     return <TextField
 *         inputRef={maskRef}
 *         value={value}
 *         onChange={(e) => handleChange(e.target.value)}
 *     />
 * }
 * ```
 */

export const useMask = (maskOptions: MaskOptions): MaskProps => {
    const {mask: maskArray, format} = maskOptions
    const [cursorPosition, setCursorPosition] = useState<CursorPosition>(null)
    const maskRef = useRef<MaskNode>(null)

    const normalizedMasks = normalizeMasks(maskArray)
    const validateMask = flattenMaskArray(normalizedMasks)
    const placeholder = getPlaceholder(normalizedMasks)

    /**
     * Moves the cursor to the passed index.
     */

    const moveCursor = (index: number) => {
        const maskNode = maskRef.current
        if (!maskNode) return
        maskNode.setSelectionRange(index, index)
    }

    /**
     * Validates the input value with a validation regex.
     */

    const validate: Validate = (value) => validateMask.test(value)

    /**
     * Handles masking for edge cases. Calls the format
     * function provided from the {@link MaskOptions mask option}
     * internally.
     */

    const mask: Mask = (value) => {
        if (!validate(value)) return value

        let newValue = value

        if (value.length === 1) {
            setCursorPosition(1)
            newValue = value + placeholder
        }

        if (value === placeholder) newValue = ''
        return format?.(newValue) ?? newValue
    }

    /**
     * Moves the cursor when hitting the
     * ArrowRight, ArrowLeft, or Backspace key.
     */

    const handleKeyDown = (event: KeyboardEvent) => {
        const validKeys = ['ArrowRight', 'ArrowLeft', 'Backspace']
        if (!validKeys.includes(event.key)) return

        const maskNode = maskRef.current
        if (!maskNode) return

        const value = maskNode.value
        if (!value) return

        const position = maskNode.selectionStart!
        const userInput = value.match(validateMask)?.slice(1)

        if (event.key === 'Backspace') {
            const simulatedValue = simulateBackspace(position, value)
            if (simulatedValue === placeholder) return
        }

        // Note: You should handle formatting the value
        //       so that it coerces to the mask.
        //       Failure to do so can cause unexpected behavior.

        if (!userInput) throw config.errors.noFormatCoerce

        const chunks = chunk(normalizedMasks, userInput)

        switch (event.key) {
            case 'ArrowRight': {
                const chunk = findChunk(chunks, position)

                if (!chunk) return
                if (chunk.type === 'user') return

                const positionOffset = chunk.value.length - 1
                return setCursorPosition(position + positionOffset)
            }

            case 'ArrowLeft':
            case 'Backspace': {
                const chunk = findChunk(chunks, position - 1)

                if (!chunk) return
                if (chunk.type === 'user') return

                const positionOffset = chunk.value.length - 1
                return setCursorPosition(position - positionOffset)
            }
        }
    }

    useEventListener('keydown', handleKeyDown, maskRef)

    useEffect(() => {
        if (!cursorPosition) return
        moveCursor(cursorPosition)
        setCursorPosition(null)
    }, [cursorPosition, maskRef])

    return {mask, placeholder, validate, maskRef}
}

export default useMask
