import {DependencyList, useEffect} from 'react'

/**
 * Creates a interval and removes it on unmount.
 *
 * Note: The number of times the interval is set/cleared
 * is dependent on the dependency list.
 *
 * @param callback A callback
 * @param ms The delay in ms
 * @param deps A {@link useEffect} dependency list
 */

export const useInterval = (
    callback: VoidFunction,
    ms = 0,
    deps?: DependencyList
) => {
    useEffect(() => {
        const interval = setInterval(callback, ms)
        return () => clearInterval(interval)
    }, deps)
}

export default useInterval
