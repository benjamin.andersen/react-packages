import {MutableRefObject} from 'react'

import useNullishRef from './useNullishRef'
import useToggle from './useToggle'
import useEventListener from './useEventListener'

export type HoverRef<T> = MutableRefObject<T | null>

/**
 * Uses a ref object to check if the specified element is hovered.
 *
 * @returns A ref object to pass to the hoverable element and a stateful hovered value.
 */

export const useHover = <T extends HTMLElement>(): [HoverRef<T>, boolean] => {
    const [hovered, toggleHovered] = useToggle()
    const hoverRef = useNullishRef<T>()

    useEventListener('mouseenter', toggleHovered, hoverRef)
    useEventListener('mouseleave', toggleHovered, hoverRef)

    return [hoverRef, hovered]
}

export default useHover
