import {useState, useEffect, DependencyList} from 'react'
import useAsyncEffect from './useAsyncEffect'
import useToggle from './useToggle'

export type ListenerCallback = () => boolean
export type LoaderCallback = () => Promise<ListenerCallback | void>

/**
 * Runs an asynchronous loader function once on mount
 * and provides a stateful 'loaded' value.
 *
 * The {@link LoaderCallback loader} function can return a {@link ListenerCallback listener} function
 * which will run whenever any of the
 * {@link DependencyList dependencies} change and should
 * return a boolean value representing the loaded state.
 *
 * @param loader The async loader function (can return a listener function).
 * @param deps A {@link useEffect} dependency list.
 * @param disable Allows to disable the loader to prevent an unnecessary re-render.
 * @returns A stateful `loaded` value.
 *
 * @example
 * ```tsx
 * // basic use case
 *
 * const loaded = useLoader(
 *     async () => {
 *         const foo = await bar()
 *     }
 * )
 *
 * // use with listener
 *
 * // this lets you customize
 * // the condition on when the
 * // loaded state should be true
 *
 * const [foo, setFoo] = useState('')
 *
 * const bar = async () => {
 *     const rand = Math.round(Math.random())
 *     return rand ? 'foo' : 'bar'
 * }
 *
 * const loaded = useLoader(
 *     async () => {
 *         const newFoo = await bar()
 *         setFoo(newFoo)
 *         // only set loaded to true if ...
 *         return () => (foo === 'foo')
 *     },
 *     // need to pass what the function above
 *     // depends on so the useEffect effect
 *     // will run the listener whenever this changes
 *     [foo]
 * )
 * ```
 */

export const useLoader = (
    loader: LoaderCallback,
    deps: DependencyList = [],
    disable = false
) => {
    const [loaded, toggleLoaded] = useToggle(disable)
    const [listener, setListener] = useState<ListenerCallback>()

    useAsyncEffect(async () => {
        if (loaded) return
        const listener = await loader()
        if (!listener) return toggleLoaded()
        setListener(listener)
    }, [])

    useEffect(() => {
        if (loaded) return
        const hasLoaded = listener?.()
        if (hasLoaded) toggleLoaded()
    }, [...deps, listener])

    return loaded
}

export default useLoader
