import {useContext, useEffect, useRef, useState, useCallback} from 'react'

import {LoadingSpinnerContext} from '../providers/LoadingSpinnerProvider'

/**
 * NOTE: This should be used in conjunction with `<LoadingSpinnerProvider />` to provide a *"global loading queue"*.
 *
 * Creates a *"local loading queue"* for the `consumer` (component) and returns stateful loading values and methods to turn on/off the loading spinner.
 *
 * `loadingSpinner.active` — is true when there is a **global** loading queue *(i.e. the loading spinner component is active)*.
 *
 * `loadingSpinner.loading` — is true when there is a **local** loading queue *(i.e. the `consumer` invoked* `loadingSpinner.on()`*)*.
 *
 * `loadingSpinner.on()` — enqueue the local and global loading queues *(i.e. enables the loading spinner)*.
 *
 * `loadingSpinner.off()` — dequeue the local and global loading queues *(this does not necessarily disable the loading spinner)*.
 *
 * @param onMount *(optional)* Automatically enable loading on mount
 */

export const useLoadingSpinner = (onMount?: boolean) => {
    const {active, addTotal} = useContext(LoadingSpinnerContext)
    const [count, setCount] = useState(0)
    const countRef = useRef(count)
    const loading = !!count

    const on = useCallback(() => {
        setCount((count) => ++count)
    }, [])

    const off = useCallback(() => {
        setCount((count) => Math.max(--count, 0))
    }, [])

    // optionally enable on mount
    // reset on unmount
    useEffect(() => {
        if (onMount) on()
        return () => addTotal(-countRef.current)
    }, [])

    // update total when possible
    useEffect(() => {
        if (countRef.current === count) return

        // calculate amount to add
        const diff = count - countRef.current
        countRef.current = count
        addTotal(diff)
    }, [count])

    return {active, loading, on, off}
}

export default useLoadingSpinner
