import {useRef, useEffect} from 'react'

/**
 * A hook that returns a boolean indicating whether this render
 * is the first render for this component.
 *
 * @returns A boolean indicating if this render is the first.
 */

export const useMountRef = () => {
    const didMountRef = useRef(false)

    useEffect(() => {
        didMountRef.current = true
    }, [])

    return didMountRef.current
}

export default useMountRef
