import {useRef} from 'react'

/**
 * Identical to {@link useRef}, except the value can be null.
 *
 * @param initialValue The initial `ref` value.
 * @returns A `useRef` object.
 */

export const useNullishRef = <T>(initialValue?: T) =>
    useRef<T | null>(initialValue ?? null)

export default useNullishRef
