import {useEffect, DependencyList} from 'react'

type AsyncEffectCallback = (isSubscribed: boolean) => Promise<void>

/**
 * Identical to {@link useEffect}, except it accepts an async effect.
 * The async effect is passed a boolean that is used to know if the async effect is currently subscribed.
 *
 * @param asyncEffect A 'useEffect' async effect.
 * @param deps A 'useEffect' dependency list.
 */

export const useAsyncEffect = (
    asyncEffect: AsyncEffectCallback,
    deps: DependencyList
) => {
    useEffect(() => {
        let isSubscribed = true
        asyncEffect(isSubscribed)

        return () => {
            isSubscribed = false
        }
    }, deps)
}

export default useAsyncEffect
