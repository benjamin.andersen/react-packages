import {DependencyList, useEffect} from 'react'

/**
 * Creates a timeout and removes it on unmount.
 *
 * Note: The number of times the timeout is set/cleared
 * is dependent on the dependency list.
 *
 * @param callback A callback
 * @param ms The delay in ms
 * @param deps A {@link useEffect} dependency list
 */

export const useTimeout = (
    callback: VoidFunction,
    ms: number,
    deps?: DependencyList
) => {
    useEffect(() => {
        const timeout = setTimeout(callback, ms)
        return () => clearTimeout(timeout)
    }, deps)
}

export default useTimeout
