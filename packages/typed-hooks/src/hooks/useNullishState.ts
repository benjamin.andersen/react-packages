import {useState} from 'react'

/**
 * Identical to {@link useState}, except the value can be null.
 *
 * @param initialState The initial `state` value.
 * @returns A stateful value and a setter function.
 */

export const useNullishState = <T>(
    initialState?: T
): [T | null, (newState?: T | null) => void] => {
    const [state, setState] = useState<T | null>(initialState ?? null)
    return [state, (newState) => setState(newState ?? null)]
}

export default useNullishState
