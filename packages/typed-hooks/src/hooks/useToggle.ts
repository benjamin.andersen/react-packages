import {useState} from 'react'

/**
 * Creates a stateful toggleable value.
 *
 * @param initialState The initial state.
 * @returns A stateful toggle value, a toggle function, and a setter.
 */

export const useToggle = (initialState = false) => {
    const [state, setState] = useState(initialState)
    const toggle = () => setState((state) => !state)
    return [state, toggle, setState] as const
}

export default useToggle
